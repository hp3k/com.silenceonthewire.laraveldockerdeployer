# Laravel + Docker + Deployer

https://github.com/laravel/laravel
https://github.com/docker
https://deployer.org/

## Usage
docker-compose up

## Config files
https://bitbucket.org/hp3k/com.silenceonthewire.laraveldockerdeployer/src/master/development/
https://bitbucket.org/hp3k/com.silenceonthewire.laraveldockerdeployer/src/master/docker

## Configuration
All configuration files are into development directory.

## Donation

If you like our solution, please send us any donation. The address associated with the account is adrian.stolarski@gmail.com
